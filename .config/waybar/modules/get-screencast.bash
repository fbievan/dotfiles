#!/bin/bash
pipewire-dump(){
  PWDUMP=$(pw-dump | jq 'map(select(.info.props."media.role" == "Screen" or select(.info.props."media.name" | . and  contains("webrtc-consume-stream"))))')
}


filterjson(){
Application=$(echo $PWDUMP | jq '.[].info.props."node.name"')
state=$(echo $PWDUMP | jq '.[].info.state')
}
pipewire-dump

start() {

if [ "$state" = "" ]; then
    echo "Nothing is being screenshared"
else
    echo "Screenshare by $Application" | tr '\n' ' '
fi
}

if [ "$PWDUMP" = "" ]; then
    echo "Connection Closed"
else
    filterjson
    start
fi

