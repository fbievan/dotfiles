# Startup in interactive
if status is-interactive
   fastfetch
   alias pan='paru'
   alias p='sxiv'
   alias dwl='dwl -s waybar'
   alias cfg='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
   alias rm='trash'
   pyenv init - | source
   alias ytm-dlp='yt-dlp -x -f m4a --audio-quality highest -o "%(album)s/%(artist)s - %(track)s.%(ext)s"'
   alias weechat='tmux -L weechat attach'
   alias wchat='tmux -L weechat attach'
   alias irc='tmux -L weechat attach'
   alias vim='nvim'
   alias fetch='fastfetch'
end
## Add bin
fish_add_path -m ~/.local/bin

## ssh agent
set SSH_AUTH_SOCK /run/user/1000/ssh-agent.socket
export SSH_AUTH_SOCK


# Add nix to path
fish_add_path ~/.nix-profile/bin


# Wayland
# export SDL_VIDEODRIVER=wayland
# export _JAVA_AWT_WM_NONREPARENTING=1
# export QT_QPA_PLATFORM=wayland
## 
# 

# make wget use XDG_BASE_DIR
alias wget 'wget --hsts-file="$XDG_DATA_HOME/wget-hsts"'

source ~/.config/env

# Added by LM Studio CLI (lms)
set -gx PATH $PATH /home/evan/.cache/lm-studio/bin

# Created by `pipx` on 2025-01-09 22:09:27
set PATH $PATH /home/evan/.local/bin
