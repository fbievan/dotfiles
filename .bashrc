#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

export HISTFILE="${XDG_STATE_HOME}"/bash/history

export NVM_DIR="$HOME/.config/nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

neofetch


# Alias lg for Looking Glass shared memory setup
# This command sequence sets up /dev/shm/looking-glass with the appropriate permissions.
alias lg='if [ ! -e /dev/shm/looking-glass ]; then \
    touch /dev/shm/looking-glass; \
    sudo chown $USER:kvm /dev/shm/looking-glass; \
    chmod 660 /dev/shm/looking-glass; \
    /usr/local/bin/looking-glass-client -S -K -1; \
else \
    /usr/local/bin/looking-glass-client -S -K -1; \
fi'

# Added by LM Studio CLI (lms)
export PATH="$PATH:/home/evan/.cache/lm-studio/bin"
