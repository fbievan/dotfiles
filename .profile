export ANDROID_HOME="$XDG_DATA_HOME"/android

export PATH="$PATH:.local/bin"

source ~/.config/env

# Added by LM Studio CLI (lms)
export PATH="$PATH:/home/evan/.cache/lm-studio/bin"
