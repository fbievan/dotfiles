#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

# Added by LM Studio CLI (lms)
export PATH="$PATH:/home/evan/.cache/lm-studio/bin"
